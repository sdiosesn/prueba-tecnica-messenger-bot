from flask import Flask, request
from pymessenger.bot import Bot
from pymessenger import Button
from musixmatch import Musixmatch
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
ACCESS_TOKEN = 'EAADUplLmpy8BAB6reO9tA87CsDVMZABE5fj9I47RYtdIgznHb6Utg6NuCbwOVWLQfCKxX046IToaIgDJ11UXMaCRGYKVZAIZCjqsfbeBv9Ej3ChZClVAZAco7GdfWUEUcRtp2lHC8xZCqwpp2VSApMNgXKesg9lkCFyagcyhS6WQZDZD'
VERIFY_TOKEN = 'ZABE5fj9I47RYtdIgzn'
bot = Bot(ACCESS_TOKEN)
musixmatch = Musixmatch('7499f51e2a1c68f2e6a8311554814d50')

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    state = db.Column(db.Integer,index=True, unique=False)

    def __repr__(self):
        return '<User: username=%r, state=%r>' % (self.username, self.state)

class Song(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    songname = db.Column(db.String(64), index=True, unique=False)
    username = db.Column(db.String(64), index=True, unique=False)

    def __repr__(self):
        return '<Song {}>'.format(self.songname)

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=False)
    text = db.Column(db.String(64), index=True, unique=False)
    worked = db.Column(db.Boolean, default=False, nullable=False)
    pub_date = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)

    def __repr__(self):
        return '<Message {}>'.format(self.text)    

@app.route("/", methods=['GET', 'POST'])
def receive_message():

	if request.method == 'GET':
		token_sent = request.args.get("hub.verify_token")
		return verify_fb_token(token_sent)
	else:	
		output = request.get_json()
		for event in output['entry']:
			messaging = event['messaging']
			for message in messaging:
				success = False
				#check if user exists
				recipient_id = message['sender']['id']
				if not User.query.filter_by(username=recipient_id).first():
					#create new user
					user = User(username=recipient_id,state=1)
					db.session.add(user)
					db.session.commit()
				user = User.query.filter_by(username=recipient_id).first()
				if message.get('message'):
					recipient_id = message['sender']['id']
					message_text = message['message'].get('text')
					if user.state == 1:
						response_sent_text = "Utilice las opciones debajo para interactuar con el bot."
						send_message(recipient_id, response_sent_text)
					if user.state == 2:
						response_sent_text = "In development"
						send_message(recipient_id, response_sent_text)
						success = True
						user.state = 1
						db.session.commit()
					if user.state == 3:
						response_sent_text = "In development"
						send_message(recipient_id, response_sent_text)
						success = True
						user.state = 1
						db.session.commit()
					if user.state == 4:
						if '-' in message_text:
							name_artist = message_text.split('-')
							name_artist[0]=name_artist[0].strip()
							name_artist[1]=name_artist[1].strip()
							response_sent_text = musixmatch.matcher_lyrics_get(name_artist[0],name_artist[1])
							if response_sent_text['message']['header']['status_code'] == 200:
								send_message(recipient_id, response_sent_text['message']['body']['lyrics']['lyrics_body'])
								success = True
								user.state = 1
								db.session.commit()
							else:
								response_sent_text = "No se encontro la canción, por favor intente de nuevo."
								send_message(recipient_id, response_sent_text)
						else:
							response_sent_text = 'Por favor ingrese el nombre de la canción y el artista deseado separado por un "-". Por ejemplo: Mentirosa-Rafaga.'
							send_message(recipient_id, response_sent_text)
					if user.state == 6:
						if '-' in message_text:
							name_artist = message_text.split('-')
							name_artist[0]=name_artist[0].strip()
							name_artist[1]=name_artist[1].strip()
							response_sent_text = musixmatch.matcher_lyrics_get(name_artist[0],name_artist[1])
							if response_sent_text['message']['header']['status_code'] == 200:
								if existsInList(name_artist, recipient_id):
									response_sent_text="Cancion ya en la lista!"
									send_message(recipient_id, response_sent_text)
									success = False
									user.state = 1
									db.session.commit()
								else:
									song = Song(songname=name_artist[0] + '-' + name_artist[1], username=recipient_id)
									db.session.add(song)
									db.session.commit()
									response_sent_text="Cancion añadida!"
									send_message(recipient_id, response_sent_text)
									success = True
								user.state = 1
								db.session.commit()
							else:
								response_sent_text = "No se encontro la canción, por favor intente de nuevo."
								send_message(recipient_id, response_sent_text)
						else:
							response_sent_text = 'Por favor ingrese el nombre de la canción y el artista deseado separado por un "-". Por ejemplo: Mentirosa-Rafaga.'
							send_message(recipient_id, response_sent_text)
					if user.state == 7:
						if message_text.isdigit():
							song_list = Song.query.filter_by(username=recipient_id).all()
							if(len(song_list) - 1 >= int(message_text)):
								#song_list.pop(int(message_text))
								db.session.delete(song_list[int(message_text)])
								db.session.commit()
								response_sent_text="Cancion removida!"
								send_message(recipient_id, response_sent_text)
								success = True
								user.state = 1
								db.session.commit()
							else:
								response_sent_text = 'Índice muy grande. Ingresar otro índice por favor.'
								send_message(recipient_id, response_sent_text)
						else:
							response_sent_text = 'Por favor ingrese índice de la canción a remover'
							send_message(recipient_id, response_sent_text)
					messageRecord = Message(username= recipient_id, text= message_text, worked= success)
					db.session.add(messageRecord)
					db.session.commit()
					#if user sends us a GIF, photo,video, or any other non-text item
					if message['message'].get('attachments'):
						response_sent_nontext = "Envie solo texto por favor."
						send_message(recipient_id, response_sent_nontext)
			for postback in messaging:
				if message.get('postback'):
					recipient_id = message['sender']['id']
					message_payload = message['postback']['payload']
					if message_payload == '<GET_STARTED_PAYLOAD>':
						#This marks that the conversation has initiated and must wait for option
						if not User.query.filter_by(username=recipient_id).first():
							#create new user
							user = User(username=recipient_id,state = 1)
							db.session.add(user)
							db.session.commit()
						user = User.query.filter_by(username=recipient_id).first()
						response_sent_text = "Bienvenido! Para buscar la letra de una canción use los botones en la barra de abajo. Puede realizar su búsqueda ingresando el nombre de la canción y el artista deseado."
						send_message(recipient_id, response_sent_text)
					elif message_payload == 'BY_SONG_NAME':
						user.state = 2
						db.session.commit()
						response_sent_text = "Ingrese el nombre de la canción deseada. Por ejemplo: Mentirosa."
						send_message(recipient_id, response_sent_text)
					elif message_payload == 'BY_ARTIST_NAME':
						user.state = 3
						db.session.commit()
						response_sent_text = "Ingrese el nombre del artista deseado. Por ejemplo: Rafaga."
						send_message(recipient_id, response_sent_text)
					elif message_payload == 'BY_BOTH_NAME':
						user.state = 4
						db.session.commit()
						response_sent_text = "Ingrese el nombre de la canción y el artista deseado separado por un '-'. Por ejemplo: Mentirosa-Rafaga."
						send_message(recipient_id, response_sent_text)
					elif message_payload == 'CHECK_LIST':
						user.state = 1
						db.session.commit()
						song_list = print_song_list(recipient_id)
						if (song_list):
							response_sent_text = song_list
							send_message(recipient_id, response_sent_text)
						else:
							response_sent_text = "No hay canciones registradas aún."
							send_message(recipient_id, response_sent_text)
					elif message_payload == 'ADD_SONG':
						user.state = 6
						db.session.commit()
						response_sent_text = "Ingrese el nombre de la canción y el artista deseado separado por un '-'. Por ejemplo: Mentirosa-Rafaga."
						send_message(recipient_id, response_sent_text)
					elif message_payload == 'REMOVE_SONG':
						user.state = 7
						db.session.commit()
						response_sent_text = print_song_list(recipient_id) + "Ingrese el índice de la canción a remover."
						send_message(recipient_id, response_sent_text)
	return "Message Processed"

def print_song_list(recipient_id):
	song_list = Song.query.filter_by(username=recipient_id).all()
	list = ""
	for idx,song in enumerate(song_list):
		list += (str(idx) + ". " + song.songname + "\n")
	return list

def verify_fb_token(token_sent):
	#take token sent by facebook and verify it matches the verify token you sent
	#if they match, allow the request, else return an error 
	if token_sent == VERIFY_TOKEN:
		return request.args.get("hub.challenge")
	return 'Invalid verification token'

def send_message(recipient_id, response):
	bot.send_text_message(recipient_id, response)
	return "success"

def existsInList(name_artist, recipient_id):
	song_list = Song.query.filter_by(username=recipient_id).all()
	for song in song_list:
		if ((song.songname).lower() == (name_artist[0].lower() + '-' + name_artist[1].lower())):
			return True
	return False

if __name__ == "__main__":
	db.create_all()
	app.run()

